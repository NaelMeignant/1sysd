#include<stdio.h>
#include<stdlib.h>


void invertcase(char *s) {
	   while (*s != '\0') {
	    	if (*s >= 'a' && *s <= 'z') {
		    	*s -= 32;
		}
		else if(*s >= 'A' && *s <= 'Z'){
			*s += 32;
		}
		s++;
	    }
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Erreur string\n");
        return 1;
    }

    invertcase(argv[1]);
    printf("%s\n", argv[1]);

    return 0;
}
