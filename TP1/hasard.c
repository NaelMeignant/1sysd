#include <stdlib.h>
#include <stdio.h>


int main(){
	int alea = rand() % 10+1;
	int devine;
	
	printf("Choisissez un nombre entre 1 et 10: ");
	scanf("%d", &devine);
	if(devine == alea){
		printf("Bravo ! Le nombre était bien %d", devine);
	}
	else if(devine < alea){
		printf("Trop petit choisissez un nombre entre 1 et 10: ");
		scanf("%d", &devine);
	}
	else if(devine > alea){
		printf("Trop grand choisissez un nombre entre 1 et 10: ");
		scanf("%d", &devine);
	}
	
}
