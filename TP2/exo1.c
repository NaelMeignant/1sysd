#include <stdlib.h>
#include <stdio.h>



float add(float a,float b){
	return a+b;
}

float sub(float a,float b){
	return a-b;
}

float mul(float a,float b){
	return a*b;
}

float divi(float a,float b){
	return a/b;
}

int main(){
	float a,b;
	char op;
	printf("Choisir premier nombre : ");
	scanf("%f", &a);
	printf("Choisir deuxième nombre : ");
	scanf("%f", &b);
	printf("Choisir l'opération à réaliser : + , - , * , / : ");
	scanf(" %c",&op);
	switch (op){
		case '+':
			printf("Addition = %.2f\n",add(a,b));
			break;
		case '-':
			printf("Soustraction = %.2f\n", sub(a,b));
			break;
		case '*':
			printf("Multiplication = %.2f\n", mul(a,b));
			break;
		case '/':
			printf("Division = %.2f\n", divi(a,b));
			break;
		default:
			printf("L'opération n'existe pas");
		
	}
	return 0;
}
