#include<stdio.h>
#include<stdlib.h>

int factorial(int n) {
	int res = 1;
	for (int i = 1; i <= n; i++) {
		res = res * i;
	}
	return res;
}

int main(int argc, char *argv[]) {
	int start;
	int end;
	    
	start = atoi(argv[1]);
	end = atoi(argv[2]);
	    
	for (int i = start; i <= end; i++) {
		printf("%d! = %d\n", i, factorial(i));
	}

	return 0;
}

// Si la fonction reçoit en paramètre 0 elle retourne 1 ce qui est correcte selon les conventions mathématiques usuelles.
