#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Point Point;
struct Point {
    char label[5]; // 4 caractère maximum
    long double x;
    long double y;
};

void display_point(Point p) {
    printf("%s(%.2Lf, %.2Lf)\n", p.label, p.x, p.y);
}

int main() {
    Point p1, p2;
    Point p3 = { "C", 23.1, 45.2 };

    strcpy(p1.label, "A");
    p1.x = 42;
    p1.y = 12;

    strcpy(p2.label, "B");
    p2.x = -12;
    p2.y = 0;

    display_point(p1);
    display_point(p2);
    display_point(p3);

    exit(EXIT_SUCCESS);
}

