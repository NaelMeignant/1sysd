#include<stdio.h>
#include<stdlib.h>

int main() {
    int guess;
    int secret;
    int essai=0;
    char prenom[20];
    srand(time(NULL));
    secret = rand() % 100 + 1 ;
    printf("Quel est ton nom ?");
    scanf("%s", &prenom);
    printf("J'ai tiré un nombre au hasard entre 1 et 100\n, essaye de le trouver !");
    while (guess != secret){
        essai++;
        printf("Aller %s choisis un nombre : ", prenom);
        scanf("%d", &guess);
        if (guess == secret) {
            printf("Bravo %s Trouvé! Tu as mis %d essais\n",prenom,essai);
        } else if (guess > secret) {
            printf("Plus petit...\n");
        } else {
            printf("Plus grand...\n");
        }
    }
    return 0;
}
